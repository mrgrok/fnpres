﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using functional.core;
using functional.core.Modules;
using LanguageExt;

namespace functional.mvc.Controllers
{
    public class AdvertsController : Controller
    {
        // POST: Adverts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MarkAsSold(int advertId)
        {
            try
            {
                var advert = AdvertsModule.GetAdvert(advertId);

                AdvertsModule.MarkAdvertAsSold(advert, SaveAdvert);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Adverts
        public ActionResult Index()
        {
            return View();
        }



        public AdvertsController(Func<Advert, Result<Unit>> SaveAdvert)
        {
            this.SaveAdvert = SaveAdvert;
        }

        private Func<Advert, Result<Unit>> SaveAdvert { get; }
    }
}