﻿using functional.core.Modules;
using LanguageExt;
using NUnit.Framework;
using System;

namespace functional.core.tests
{
    [TestFixture]
    public class AdvertsModuleTests
    {
        [Test]
        public void MarkAsSold_Should_Set_IsSold_On_Returned_Advert()
        {
            var isSold = false;

            var advert = new Advert(
                title: "title", 
                description: "description", 
                price: 10,
                isSold: isSold
            );

            var saveAdvertResult = AdvertsModule.MarkAdvertAsSold(advert, (_) => Unit.Default);

            Assert.IsTrue(saveAdvertResult.Match(
                a => a.IsSold, // save operation succeeded, test the value of isSold
                _ => false // save failed, force to false
            ));
        }

        [Test]
        public void MarkAsSold_Should_Not_Change_IsSold_On_Original_Advert_Passed_In() // this is a little bit overkill really
        {
            var isSold = false;

            var advert = new Advert(
                title: "title",
                description: "description",
                price: 10,
                isSold: isSold
            );

            var saveAdvertResult = AdvertsModule.MarkAdvertAsSold(advert, (_) => Unit.Default);

            Assert.AreEqual(isSold, advert.IsSold); // make sure the original value wasn't messed with
        }

        [Test]
        public void MarkAsSold_Should_Call_Save()
        {
            var isSold = false;
            var saveCalled = false;

            var advert = new Advert(
                title: "title",
                description: "description",
                price: 10,
                isSold: isSold
            );

            var updatedAdvert = AdvertsModule.MarkAdvertAsSold(advert, (_) => { saveCalled = true; return Unit.Default; });

            Assert.IsTrue(saveCalled);
        }
    }
}
