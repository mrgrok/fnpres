using functional.core;
using LanguageExt;
using System;

namespace functional.data
{
    public static class Db
    {
        public static Result<Unit> SaveAdvert(Advert advert)
        {
            // save advert

            // if it failed we would do...:
            // return new Result<Unit>(new Exception("some failure as an exception... interesting..."));

            // or even 
            //try { /* */  }
            //catch (SomeException ex) { return ex; } }
            // which is an interesting way to do away with exceptions

            // of course we probably would have been better returning an Advert from this function anyway - maybe 
            // the Advert Id was updated... if so, maybe we should communicate that back to the caller.

            // assume success
            return Unit.Default;
        }
    }
}