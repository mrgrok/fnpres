namespace functional.core
{
    public class Advert
    {
        public bool IsSold { get; }
        public string Title { get; } = "";
        public string Description { get; } = "";
        public int Price { get; }

        public Advert(string title, string description, int price, bool isSold = false)
        {
            Title = title;
            Description = description;
            Price = price;
            IsSold = isSold;
        }

        public Advert With(string title = null, string description = null, int? price = null, bool? isSold = null)
        {
            return new Advert(
                title: title ?? this.Title,
                description: description ?? this.Description,
                price: price ?? this.Price,
                isSold: isSold ?? this.IsSold
            );
        }
    }
}