﻿using LanguageExt;
using System;
using System.Collections.Generic;
using System.Text;

namespace functional.core.Modules
{
    public static class AdvertsModule
    {
        public static Result<Advert> MarkAdvertAsSold(Advert advert, Func<Advert, Result<Unit>> SaveAdvert)
        {
            var updatedAdvert = advert.With(isSold: true);

            return SaveAdvert(updatedAdvert).Map(
                _ => updatedAdvert // map Unit (the void from db layer to the updated advert we supplied)
                // this is where map is doing some magic
                // map allows us to act as if we're talking about the unelevated type (a Unit) rather than a Result<Unit>
                // and then it will wrap it back up into a Result<TReturn> for us based on what we return
                // So we gave it a function that turns a Unit -> Advert and it did the dirty work to wrap and unwrap the result
                // as required
            );
        }

        public static Advert MarkAsSold(Advert advert, Action<Advert> SaveAdvert)
        {
            var updatedAdvert = advert.With(isSold: true);

            SaveAdvert(updatedAdvert);

            return updatedAdvert;
        }

        public static Advert GetAdvert(int advertId)
        {
            return new Advert(
                title: "blah",
                description: "blah blah blahdy blah blah blah...",
                price: 100
            );
        }
    }
}
