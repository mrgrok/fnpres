# fnpres

Basic example to compare an OO style project with a functional project in C#. Lacks the actual connection to a database which of course the db method would need. 

Introduces the concepts of programming entirely with static methods, Result, and Map.

Provides an example of an immutable type with all the nasty boilerplate required - (we probably won't get Records and native With method till C#9).

Shows one way to inject of Controllers with Funcs.


## Improvements that could be made (already!):

Composition should probably be called from StartUp.cs rather than written directly in there.

I might flesh this example out a bit more so it has actually has a proper backing store and then you have to think about things like where the db command 
would get it's connection from and how we might go about hiding away the details of wrapping the command in a using statement.

