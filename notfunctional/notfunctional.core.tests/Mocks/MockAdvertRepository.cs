﻿using System;
using System.Collections.Generic;
using System.Text;

namespace notfunctional.core.tests.Mocks
{
    public class MockAdvertRepository : IAdvertRepository
    {
        public bool RemoveCalled { get; private set; }
        public bool SaveCalled { get; private set; }

        public bool Remove(Advert advert)
        {
            RemoveCalled = true;

            return true;
        }

        public void Save(Advert advert)
        {
            SaveCalled = true;
        }
    }
}
