﻿using notfunctional.core.tests.Mocks;
using NUnit.Framework;
using System;

namespace notfunctional.core.tests
{
    [TestFixture]
    public class AdvertServiceTests
    {
        [Test]
        public void MarkAsSold_Should_Set_IsSold()
        {
            var isSold = false;

            var advert = new Advert(
                title: "title", 
                description: "description", 
                price: 10,
                isSold: isSold
            );

            var mockAdvertRepository = new MockAdvertRepository();
            var advertService = new AdvertService(mockAdvertRepository);

            var updatedAdvert = advertService.MarkAsSold(advert);

            Assert.AreEqual(isSold, advert.IsSold);
            Assert.IsTrue(updatedAdvert.IsSold);
        }

        [Test]
        public void MarkAsSold_Should_Save_To_Repository()
        {
            var isSold = false;

            var advert = new Advert(
                title: "title",
                description: "description",
                price: 10,
                isSold: isSold
            );

            var mockAdvertRepository = new MockAdvertRepository();
            var advertService = new AdvertService(mockAdvertRepository);

            var updatedAdvert = advertService.MarkAsSold(advert);

            Assert.IsTrue(mockAdvertRepository.SaveCalled);
        }
    }
}
