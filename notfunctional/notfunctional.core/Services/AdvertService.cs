namespace notfunctional.core
{
    public class AdvertService : IAdvertService
    {
        public IAdvertRepository AdvertRepository = null;
        
        public Advert MarkAsSold(Advert advert)
        {
            var updatedAdvert = advert.With(isSold: true);
            
            AdvertRepository.Save(updatedAdvert);
            
            return(updatedAdvert);
        }

        public Advert GetAdvert(int advertId)
        {
            return new Advert(
                title: "blah", 
                description: "blah blah blahdy blah blah blah...", 
                price: 100
            );
        }

        public AdvertService(IAdvertRepository advertRepository)
        {
            // fingers crossed it isn't null...
            AdvertRepository = advertRepository;
        }
    }
}