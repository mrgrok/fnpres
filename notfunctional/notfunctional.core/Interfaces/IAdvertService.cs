namespace notfunctional.core
{
    public interface IAdvertService
    {
        Advert MarkAsSold(Advert advert);
        Advert GetAdvert(int advertId);
    }
}