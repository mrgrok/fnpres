namespace notfunctional.core
{
    public interface IAdvertRepository
    {
        void Save(Advert advert);
        bool Remove(Advert advert);
    }
}