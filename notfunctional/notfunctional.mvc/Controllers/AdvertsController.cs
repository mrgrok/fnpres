﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using notfunctional.core;

namespace notfunctional.mvc.Controllers
{
    public class AdvertsController : Controller
    {
        // POST: Adverts/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MarkAsSold(int advertId)
        {
            try
            {
                // get the advert
                var advert = AdvertService.GetAdvert(advertId);

                AdvertService.MarkAsSold(advert);

                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Adverts
        public ActionResult Index()
        {
            return View();
        }



        public AdvertsController(IAdvertService advertService)
        {
            AdvertService = advertService;
        }

        private IAdvertService AdvertService;
    }
}